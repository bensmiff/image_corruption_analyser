from __future__ import print_function
from PIL import Image
from PIL import ImageDraw
import time
import os
from os import listdir
from os.path import isfile, join
import logging

###   Editable values

ANALYSIS_INTERVAL = 1               # the interval between pixels to be compared.
                                    # 1 = compare every pixel between the two lines, 10 = compare pixels 0, 10, 20, 30 etc
                                    # the higher the number the faster but less reliable the analysis is
ANALYSIS_THRESHOLD_PERCENT = 0.1    # percentage difference between two lines for it to be considered abnormal/corrupted
ANALYSIS_THRESHOLD_OCCURANCES = 4   # how many times the threshold needs to be exceeded to call the image corrupted
ANALYSIS_SEPARATION = 25            # number of lines to ignore after the threshold has been exceeded
                                    # used to stop the same spike registering multiple times

OUTPUT_IMAGE = True                 # if True an image will be made showing a visualisation of comparison_results
                                    # light grey is the threshold and the black lines show where it has been exceeded
OUTPUT_IMAGE_WIDTH = 400            # Width of optional output image, height is the same as input image

DIRECTORY_PATH = os.path.dirname(os.path.abspath(__file__)) + "/images/" # path of directory to containing images to be analysed

###


logger = logging.getLogger('logger')
hdlr = logging.FileHandler(DIRECTORY_PATH + '/corrupted.log')
formatter = logging.Formatter('%(message)s')
hdlr.setFormatter(formatter)
logger.setLevel(logging.DEBUG)
logger.addHandler(hdlr)

WIDTH = 0
HEIGHT = 1


class CorruptionAnalysis(object):
    """ CorruptionAnalysis class detecting signs of corruption
    """

    comparison_results = []     # results from compare_lines between index and index + 1
    image_size = [0, 0]         # size of input image

    def __init__(self):
        pass

    def set_image_size(self, image):
        self.image_size = image.size
        self.comparison_results = [None] * image.size[HEIGHT]

    def calculate_row_diffs_list(self, px, start_index, end_index):
        #print("Calculating row diffs: Start", start_index, "End", end_index)
        for index in range(start_index, end_index):
            comparison_with_next = self.compare_lines(px, self.image_size[WIDTH], index, index + 1)
            self.comparison_results[index] = comparison_with_next


    def compare_lines(self, px, row_width, row1, row2):

        #print("Comparing row: ", row1, row2)

        row_diff = 0
        for x in range(0, row_width, ANALYSIS_INTERVAL):
            #Image.getpixel(xy) is an alternative but is slower

            row1_pixel = px[x, row1]
            row2_pixel = px[x, row2]
            row1_pixel_average = sum(row1_pixel)/len(row1_pixel)
            row2_pixel_average = sum(row2_pixel)/len(row2_pixel)
            diff = abs(row1_pixel_average - row2_pixel_average)
            row_diff += diff

        #print("Compared row: ", row1, row2, "Result:", row_diff)
        return row_diff


    def analyse_row_diffs(self, filepath):

        # for a line, work out the maximum possible difference value
        max_diff = 255 * (self.image_size[WIDTH] / ANALYSIS_INTERVAL)

        threshold_amount = ANALYSIS_THRESHOLD_PERCENT * max_diff


        if OUTPUT_IMAGE:
            # Setup output image
            size = OUTPUT_IMAGE_WIDTH, self.image_size[HEIGHT]
            max_val = max(threshold_amount * 1.1, max(self.comparison_results))
            output_image = Image.new('L', size, color=255)
            draw = ImageDraw.Draw(output_image)
            threshold_output_percent = (float(threshold_amount) / float(max_val))
            draw.rectangle([0, 0, threshold_output_percent * OUTPUT_IMAGE_WIDTH, self.image_size[HEIGHT]], fill=235)


        threshold_exceeded = 0
        separation_counter = 0

        for idx, row in enumerate(self.comparison_results):
            if row is not None:

                if separation_counter == 0 and float(row)> threshold_amount:
                    threshold_exceeded += 1
                    separation_counter = ANALYSIS_SEPARATION

                if OUTPUT_IMAGE:
                    # draw lines to output image
                    percent = (float(row) / float(max_val))
                    colour = 0 if separation_counter > 0 else 150
                    draw.line((0, idx) + (percent * OUTPUT_IMAGE_WIDTH, idx), fill=colour)

                separation_counter = max(0, separation_counter - 1)

        corrupted = threshold_exceeded >= ANALYSIS_THRESHOLD_OCCURANCES

        print("Exceeded", ANALYSIS_THRESHOLD_PERCENT, "threshold", threshold_exceeded, "times")
        print("Image Corrupted:", corrupted)

        if corrupted:
            logger.info(filepath)

        if OUTPUT_IMAGE:
            # Save output image
            del draw
            output_image.save(filepath + '_output.png')

        return



def main():

        filepaths = [f for f in listdir(DIRECTORY_PATH) if isfile(join(DIRECTORY_PATH, f)) and "_output" not in f]

        for filepath in filepaths:

            filepath = DIRECTORY_PATH + filepath
            print('-------------------')
            print('Analysing:', filepath)

            try:
                with Image.open(filepath) as source_image:

                    print(source_image.format, "%dx%d" % source_image.size, source_image.mode)
                    print('')

                    time_start = time.time()
                    analyser = CorruptionAnalysis()
                    analyser.set_image_size(source_image)
                    px = source_image.load()

                    analyser.calculate_row_diffs_list(px, 0, source_image.height - 1)
                    analyser.analyse_row_diffs(filepath)

                    time_elapsed = time.time() - time_start
                    print('\nTime Elapsed: %d seconds' % time_elapsed)
            except IOError as error:
                print('-------------------')
                print('Filepath not found, or file is not an image')
                print('-------------------')


if __name__ == '__main__':
    main()





